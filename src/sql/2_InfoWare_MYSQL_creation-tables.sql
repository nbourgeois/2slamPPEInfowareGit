-- phpMyAdmin SQL Dump
-- version 3.4.2
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le : Lun 19 Mars 2012 à 17:35
-- Version du serveur: 5.1.55
-- Version de PHP: 5.3.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `InfoWare`
--

-- --------------------------------------------------------

--
-- Structure de la table `Categorie`
--

CREATE TABLE IF NOT EXISTS `Categorie` (
  `Code` varchar(2) NOT NULL DEFAULT '',
  `Libelle` varchar(16) DEFAULT NULL,
  `salaireMini` double DEFAULT NULL,
  `CaisseDeRetraite` varchar(5) DEFAULT NULL,
  `PrimeResultat` int(1) DEFAULT NULL,
  PRIMARY KEY (`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Categorie`
--

INSERT INTO `Categorie` (`Code`, `Libelle`, `salaireMini`, `CaisseDeRetraite`, `PrimeResultat`) VALUES
('C1', 'Cadre moyen', 1900, 'AGIRC', 1),
('C2', 'Cadre supérieur', 2500, 'AGIRC', 1),
('E1', 'Employé niveau 1', 1250, 'ARRCO', 0),
('E2', 'Employé niveau 2', 1500, 'ARRCO', 0);

-- --------------------------------------------------------

--
-- Structure de la table `Formation`
--

CREATE TABLE IF NOT EXISTS `Formation` (
  `Code` varchar(3) NOT NULL DEFAULT '',
  `Nom` varchar(44) DEFAULT NULL,
  `DateDebut` date DEFAULT NULL,
  `Nbrejours` int(1) DEFAULT NULL,
  `CoutJourForm` double DEFAULT NULL,
  PRIMARY KEY (`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Formation`
--

INSERT INTO `Formation` (`Code`, `Nom`, `DateDebut`, `Nbrejours`, `CoutJourForm`) VALUES
('F01', 'Visual Basic débutant', '2006-02-05', 4, 800),
('F02', 'Visual Basic initié', '2006-05-20', 5, 800),
('F03', 'Administration d''un réseau local sous Linux', '2006-09-14', 6, 900),
('F04', 'Développer en PHP', '2006-05-10', 4, 800),
('F05', 'La réalisation d''un projet informatique', '2006-05-17', 3, 1300),
('F06', 'SQL-Server', '2006-02-25', 4, 1000),
('F07', 'Ciel paye', '2006-03-14', 3, 700),
('F08', 'Anglais débutant', '2006-02-25', 8, 400),
('F09', 'Anglais avancé', '2006-04-05', 3, 1100),
('F10', 'Relations clientèle', '2006-05-08', 3, 800),
('F11', 'La fiscalité des PME', '2006-10-17', 2, 1000),
('F12', 'Fixer des objectifs à votre force de vente', '2006-10-20', 1, 850),
('F13', 'Optimiser son portefeuille clients', '2006-01-02', 3, 600),
('F14', 'Utiliser un logiciel de gestion de clientèle', '2006-02-09', 5, 900);

-- --------------------------------------------------------

--
-- Structure de la table `Salarie`
--

CREATE TABLE IF NOT EXISTS `Salarie` (
  `Code` varchar(3) NOT NULL DEFAULT '',
  `Nom` varchar(7) DEFAULT NULL,
  `Prenom` varchar(8) DEFAULT NULL,
  `DateNaiss` date DEFAULT NULL,
  `DateEmbauche` date DEFAULT NULL,
  `Fonction` varchar(23) DEFAULT NULL,
  `TauxHoraire` double DEFAULT NULL,
  `situationFamiliale` varchar(11) DEFAULT NULL,
  `NbrEnfants` int(1) DEFAULT NULL,
  `NumCat` varchar(2) DEFAULT NULL,
  `CodeServ` int(3) DEFAULT NULL,
  PRIMARY KEY (`Code`),
  KEY `CodeServ` (`CodeServ`),
  KEY `NumCat` (`NumCat`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Salarie`
--

INSERT INTO `Salarie` (`Code`, `Nom`, `Prenom`, `DateNaiss`, `DateEmbauche`, `Fonction`, `TauxHoraire`, `situationFamiliale`, `NbrEnfants`, `NumCat`, `CodeServ`) VALUES
('S01', 'RETAIS', 'Claude', '1962-03-31', '2000-09-01', 'Chef de projet', 19.5, 'Marié', 3, 'C2', 1),
('S02', 'BERNARD', 'Céline', '1972-08-14', '2000-09-01', 'Directrice commercial', 19.5, 'Mariée', 2, 'C2', 3),
('S03', 'RETAIS', 'Jérôme', '1968-09-14', '2000-09-01', 'Ingénieur informatique', 14, 'Divorcé', 4, 'C1', 1),
('S04', 'DOMARD', 'Pierre', '1960-06-14', '2000-09-01', 'Directeur général', 22.5, 'Marié', 3, 'C2', 2),
('S05', 'LALOIS', 'Régis', '1967-07-25', '2000-09-01', 'Chef comptable', 17.3, 'Célibataire', 0, 'C2', 2),
('S06', 'DUPONT', 'Henri', '1968-11-15', '2000-09-01', 'Développeur', 11.6, 'Marié', 2, 'E2', 1),
('S07', 'AJAVAR', 'Karima', '1981-11-25', '2001-08-01', 'Standardiste', 8.2, 'Célibataire', 1, 'E1', 2),
('S08', 'FALERT', 'Maud', '1980-08-25', '2002-01-01', 'Développeur', 10.3, 'Mariée', 2, 'E2', 1),
('S09', 'MALE', 'Emilie', '1980-01-14', '2002-02-01', 'Assistant Comptable', 8.7, 'Divorcée', 2, 'E1', 2),
('S10', 'MOUDA', 'Mustapha', '1958-01-14', '2002-05-01', 'Ingénieur Commercial', 12.6, 'Marié', 3, 'C1', 3),
('S11', 'AJAVAR', 'Médhi', '1978-02-14', '2002-06-01', 'Développeur', 10, 'Marié', 4, 'E2', 1),
('S12', 'WANG', 'Vinthan', '1980-07-22', '2002-06-01', 'Développeur', 10, 'Marié', 1, 'E2', 1),
('S13', 'SAVOY', 'Marilyne', '1980-07-16', '2003-01-01', 'Secrétaire Commerciale', 8.7, 'Mariée', 3, 'E1', 3),
('S14', 'ESTOUDE', 'Sophie', '1976-08-17', '2003-01-01', 'Assistante de direction', 10, 'Divorcée', 2, 'E2', 2),
('S15', 'PETIT', 'Sylvie', '1979-05-28', '2003-01-01', 'Technico Commercial', 10, 'Mariée', 0, 'E2', 3),
('S16', 'GIRARDO', 'Pablo', '1980-08-14', '2003-05-01', 'Administrateur réseau', 9.1, 'Célibataire', 1, 'E1', 1),
('S17', 'ZOARD', 'Farid', '1983-07-23', '2003-10-01', 'Développeur-Stagiaire', 8.5, 'Célibataire', 0, 'E1', 1);

-- --------------------------------------------------------

--
-- Structure de la table `Service`
--

CREATE TABLE IF NOT EXISTS `Service` (
  `Code` int(1) NOT NULL DEFAULT '0',
  `Designation` varchar(13) DEFAULT NULL,
  `Email` varchar(27) DEFAULT NULL,
  `Tel` int(10) DEFAULT NULL,
  PRIMARY KEY (`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Service`
--

INSERT INTO `Service` (`Code`, `Designation`, `Email`, `Tel`) VALUES
(1, 'Informatique', 'Inf-logihome@logihome.com', 169983212),
(2, 'Adminstration', 'Admin-logihome@logihome.com', 169983210),
(3, 'Commercial', 'Com-Logihome@logihome.com', 169983215),
(4, 'Comptable', ' ', 169983218);

-- --------------------------------------------------------

--
-- Structure de la table `Suivre`
--

CREATE TABLE IF NOT EXISTS `Suivre` (
  `CodeSal` varchar(3) NOT NULL DEFAULT '',
  `CodeForm` varchar(3) NOT NULL DEFAULT '',
  PRIMARY KEY (`CodeSal`,`CodeForm`),
  KEY `CodeSal` (`CodeSal`),
  KEY `CodeForm` (`CodeForm`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Suivre`
--

INSERT INTO `Suivre` (`CodeSal`, `CodeForm`) VALUES
('S01', 'F04'),
('S02', 'F10'),
('S02', 'F12'),
('S03', 'F05'),
('S05', 'F11'),
('S06', 'F01'),
('S06', 'F02'),
('S07', 'F08'),
('S09', 'F07'),
('S10', 'F08'),
('S11', 'F04'),
('S13', 'F14'),
('S14', 'F09'),
('S15', 'F13'),
('S16', 'F03'),
('S17', 'F06');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `Salarie`
--
ALTER TABLE `Salarie`
  ADD CONSTRAINT `Salarie_ibfk_1` FOREIGN KEY (`NumCat`) REFERENCES `Categorie` (`Code`),
  ADD CONSTRAINT `Salarie_ibfk_2` FOREIGN KEY (`CodeServ`) REFERENCES `Service` (`Code`);

--
-- Contraintes pour la table `Suivre`
--
ALTER TABLE `Suivre`
  ADD CONSTRAINT `Suivre_ibfk_2` FOREIGN KEY (`CodeForm`) REFERENCES `Formation` (`Code`),
  ADD CONSTRAINT `Suivre_ibfk_1` FOREIGN KEY (`CodeSal`) REFERENCES `Salarie` (`Code`);
