-- Base de données: InfoWare
--
CREATE DATABASE InfoWare DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE InfoWare;
CREATE USER 'infoware_util'@'localhost' IDENTIFIED BY 'secret';
GRANT ALL PRIVILEGES ON InfoWare.* to 'infoware_util'@'localhost';

