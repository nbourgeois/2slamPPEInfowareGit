/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test.gui;

import gui.JFrameListeSalaries;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import modele.dao.DaoSalarie;
import modele.dao.DaoService;
import modele.metier.Salarie;
import modele.metier.Service;

/**
 *
 * @author btssio
 */
public class TestJFrameListeSalaries {

    public static void main(String args[]) {
        JFrameListeSalaries jfListeSalaries;
        ArrayList<Service> lesServices = null;
        ArrayList<Salarie> lesSalaries = null;

        jfListeSalaries = new JFrameListeSalaries();
        jfListeSalaries.setVisible(true);

        // Remplissage combobox des services
        try {
            lesServices = DaoService.getAll();
        } catch (SQLException ex) {
            System.out.println("TestJFrameListeSalaries - pb remplissage combo services : " + ex.getMessage());
        }
        // BOUCHON 
//        lesServices = new ArrayList<>();
//        lesServices.add(new Service(1, "Informatique"));
//        lesServices.add(new Service(2, "Administration"));
//        lesServices.add(new Service(3, "Commercial"));
//        lesServices.add(new Service(4, "Comptable"));
        jfListeSalaries.remplirJComBoxServices(lesServices);

        // Remplissage jtable des salariés avec
        try {
            int codeServiceSelectionne = lesServices.get(0).getCode();
            lesSalaries = DaoSalarie.getAllByService(codeServiceSelectionne);
        } catch (SQLException ex) {
            System.out.println("TestJFrameListeSalaries - pb remplissage JTable salaries : " + ex.getMessage());
        }
        // BOUCHON
//        lesSalaries = new ArrayList<>();        
//        lesSalaries.add(new Salarie("S01", "RETAIS", "Claude", Date.valueOf("1962-03-31"), Date.valueOf("2000-09-01"), "Chef de projet", 19.5, "Marié", 3));
//        lesSalaries.add(new Salarie("S02", "BERNARD", "Céline", Date.valueOf("1972-08-14"), Date.valueOf("2000-09-01"), "Directrice commerciale", 19.5, "Marié", 2));
//        lesSalaries.add(new Salarie("S03", "RETAIS", "Jérôme", Date.valueOf("1968-09-14"), Date.valueOf("2000-09-01"), "Ingénieur informatique", 14.0, "Divorcé", 4));
//        lesSalaries.add(new Salarie("S04", "DOMARD", "Pierre", Date.valueOf("1960-06-14"), Date.valueOf("2000-09-01"), "Directeur général", 22.5, "Marié", 3));
//        lesSalaries.add(new Salarie("S05", "LALOIS", "Régis", Date.valueOf("1967-07-25"), Date.valueOf("2000-09-01"), "Chef comptable", 17.3, "Célibataire", 0));
//        lesSalaries.add(new Salarie("S06", "DUPONT", "Henri", Date.valueOf("1968-11-15"), Date.valueOf("2000-09-01"), "Développeur", 11.6, "Marié", 2));
//        lesSalaries.add(new Salarie("S07", "AJAVAR", "Karima", Date.valueOf("1981-11-25"), Date.valueOf("2001-08-01"), "Standardiste", 8.2d, "Célibataire", 1));
//        lesSalaries.add(new Salarie("S08", "FALERT", "Maud", Date.valueOf("1980-08-25"), Date.valueOf("2002-09-01"), "Développeur", 10.3d, "Mariée", 2));                              
        jfListeSalaries.remplirJTableSalaries(lesSalaries);

    }
}
